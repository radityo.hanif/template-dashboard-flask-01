import os
import inspect
from ...Dashboard.sidebar import sidebar
from flask import Blueprint, render_template

Pegawai = Blueprint (
    name='Pegawai', 
    import_name=__name__,   
    url_prefix='/pegawai',
    template_folder='../../../templates/pages/Pegawai'
)

prefix = "pgw_"
app_name = os.getenv("APP_NAME")
blueprint_name = "Pegawai"
title = f"Dashboard - {blueprint_name} {app_name}"

data = dict()
data["sidebar"] = sidebar
data["active_sidebar"] = blueprint_name

@Pegawai.get("/")
def index():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name

    data["js_src"] = f"js/pages/{blueprint_name}/{template_name}.js"
    data["enable_extensions"] = ["datatables"]
    
    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )