import os
import inspect
from .sidebar import sidebar
from flask import current_app as app
from flask import Blueprint, render_template, request

Dashboard = Blueprint (
    name="Dashboard", 
    import_name=__name__,   
    url_prefix="/dashboard",
    template_folder="../../templates/pages/Dashboard"
)

prefix = "db_"
app_name = os.getenv("APP_NAME")
blueprint_name = "Dashboard"
title = f"Dashboard - {app_name}"

data = dict()
data["sidebar"] = sidebar
data["active_sidebar"] = blueprint_name

    
@Dashboard.get("/")
def index():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name

    data["js_src"] = f"js/pages/{blueprint_name}/{template_name}.js"
    data["enable_extensions"] = ["sweetalert"]
    
    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )

@Dashboard.get("/logout")
def logout():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name

    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )

# Import Blueprint
from .User import User
from .File import File
from .Pegawai import Pegawai

# Register Blueprint
Dashboard.register_blueprint(User)
Dashboard.register_blueprint(File)
Dashboard.register_blueprint(Pegawai)