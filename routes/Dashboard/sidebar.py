sidebar = [
    {
        "title" : "Menu",
        "data" : [
            # Dashboard
            {
                "title" : "Dashboard",
                "type" : "text",
                "link" : "/dashboard",
                "icon" : "bi bi-grid-fill",
            },

            # Pegawai
            {
                "title" : "Pegawai",
                "type" : "dropdown",
                "icon" : "bi bi-stack",
                "data" : [
                    # Datatables
                    {
                        "title" : "Datatables",
                        "type" : "text",
                        "link" : "/dashboard/pegawai"
                    },
                    # Menu Lainnya
                    {
                        "title" : "Menu Lainnya",
                        "type" : "text",
                        "link" : "/dashboard/pegawai"
                    }
                ]
            }, 
            
            # User
            {
                "title" : "User",
                "type" : "dropdown",
                "icon" : "bi bi-people-fill",
                "data" : [
                    # Manajemen data
                    {
                        "title" : "Manajemen data",
                        "type" : "text",
                        "link" : "/dashboard/user"
                    },
                ]
            }, 
            
            # File
            {
                "title" : "File",
                "type" : "dropdown",
                "icon" : "bi bi-folder-fill",
                "data" : [
                    # My File
                    {
                        "title" : "My File",
                        "type" : "text",
                        "link" : "/dashboard/file"
                    },
                    # Upload File
                    {
                        "title" : "Upload File",
                        "type" : "text",
                        "link" : "/dashboard/file/upload"
                    },
                ]
            }, 
        ]
    },
    {
        "title" : "Menu Lainnya",
        "data" : [
            # Pengaturan
            {
                "title" : "Pengaturan",
                "type" : "text",
                "link" : "/dashboard",
                "icon" : "bi bi-gear-fill",
            },
            
            # Logout
            {
                "title" : "Logout",
                "type" : "text",
                "link" : "/dashboard/logout",
                "icon" : "bi bi-box-arrow-left",
            },
        ]
    },
]