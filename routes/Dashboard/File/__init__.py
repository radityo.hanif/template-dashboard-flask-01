import os
import inspect
from ...Dashboard.sidebar import sidebar
from flask import Blueprint, render_template

File = Blueprint (
    name='File', 
    import_name=__name__,   
    url_prefix='/file',
    template_folder='../../../templates/pages/File'
)

prefix = "file_"
app_name = os.getenv("APP_NAME")
blueprint_name = "File"
title = f"Dashboard - {blueprint_name} {app_name}"

data = dict()
data["sidebar"] = sidebar
data["active_sidebar"] = blueprint_name

@File.get("/")
def index():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name

    data["js_src"] = f"js/pages/{blueprint_name}/{template_name}.js"
    data["enable_extensions"] = ["datatables", "sweetalert"]
    
    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )

@File.get("/upload")
def upload():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name

    data["js_src"] = f"js/pages/{blueprint_name}/{template_name}.js"
    data["enable_extensions"] = ["sweetalert"]
    
    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )