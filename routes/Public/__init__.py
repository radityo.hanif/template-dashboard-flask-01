import os
import inspect
from flask import current_app as app
from flask import Blueprint, render_template, request

Public = Blueprint (
    name="Public", 
    import_name=__name__,   
    url_prefix="/",
    template_folder="../../templates/pages/Public"
)

prefix = "public_"
app_name = os.getenv("APP_NAME")
title = f"{app_name}"
blueprint_name = "Public"
data = dict()

@Public.get("/")
def index():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name  
    
    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )

@Public.get("/login")
def login():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name  
    
    data["js_src"] = f"js/pages/{blueprint_name}/{template_name}.js"
    data["enable_extensions"] = ["sweetalert"]

    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )

@Public.get("/register")
def register():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name  
    
    data["js_src"] = f"js/pages/{blueprint_name}/{template_name}.js"
    data["enable_extensions"] = ["sweetalert"]

    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )

@Public.get("/activation")
def activation():
    frame = inspect.currentframe()
    function_name = frame.f_code.co_name
    template_name = prefix + function_name  
    
    data["js_src"] = f"js/pages/{blueprint_name}/{template_name}.js"
    data["enable_extensions"] = ["sweetalert"]

    return render_template (
        data=data,
        title=title,
        template_name_or_list= f"{template_name}.html",
    )