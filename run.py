import os
from flask import Flask, render_template
from dotenv import load_dotenv

load_dotenv()
app = Flask(__name__)

# Register env variables
app.config["IS_PROD"] = os.getenv("IS_PROD")
app.config["BASE_URL"] = os.getenv("BASE_URL")
app.config["APP_NAME"] = os.getenv("APP_NAME")
app.config["BACKEND_URL"] = os.getenv("BACKEND_URL")

# Import blueprint
from routes.Public import Public
from routes.Dashboard import Dashboard

# Register blueprint
app.register_blueprint(Public)
app.register_blueprint(Dashboard)

# Error page handler
@app.errorhandler(404)
def page_not_found(e):
    return render_template('layouts/error/404.html')

@app.errorhandler(403)
def page_forbidden(e):
    return render_template('layouts/error/403.html')

# Run flask web server
if __name__ == '__main__':
    app.run(debug=True, port=os.getenv("APP_PORT"))