localStorage.setItem("isLoading", 0);

function showLoading() {
    let isLoading = localStorage.getItem("isLoading");
    if(isLoading == 0) {
        $('#loading').removeClass('d-none');
    }
    localStorage.setItem("isLoading", 1);
}

function hideLoading() {
    let isLoading = localStorage.getItem("isLoading");
    if(isLoading == 1) {
        $('#loading').addClass('d-none');
    }
    localStorage.setItem("isLoading", 0);
}
