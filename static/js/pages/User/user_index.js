let payload;

// Inisiasi Datatables
const datatables = $("#table-index-user").DataTable({
    serverSide: true,
    pageLength: 10,
    searching: true,
    processing: true,
    columnDefs: [{
        defaultContent: "-",
        targets: "_all"
    }],
    ajax: {
        type: "POST",
        url: api("/user"),
        headers: {
            'Authorization': getBearerToken()
        },
        data: payload,
        beforeSend: function () {
            showLoading();
        },
        error: function (response) {
            hideLoading();
            let res, errorMessage;
            res = response.responseJSON;
            if (res.message) {
                errorMessage = res.message;
            } else {
                errorMessage = 'Terjadi kesalahan pada sistem, silahkan hubungi admin';
            }
            showErrorMessage(errorMessage);
            console.log({ error: response });
        }
    },
    columns: [
        {
            data: "login_name"
        },
        {
            data: "login_email"
        },
        {
            data: "updated_at"
        },
        {
            data: null,
            render: function (data) {
                let idx = `id="status-data-${data.id}"`;
                if (data.deleted_at == null) {
                    return `<span ${idx} class="badge text-bg-primary">Aktif</span>`
                } else {
                    return `<span ${idx} class="badge text-bg-danger">Non Aktif</span>`
                }
            }
        },
        {
            data: null,
            render: function (data) {
                let buttons = `
                    <a href="detail?id=${data.id}" class="btn btn-outline-primary">
                        <i class="bi bi-eye-fill"></i>
                        Detail
                    </a>
                    
                    <a href="edit?id=${data.id}" class="btn btn-outline-success">
                        <i class="bi bi-pencil-fill"></i>
                        Edit
                    </a>
                `;

                // Jika data belum dihapus, tampilkan tombol hapus
                if (data.deleted_at == null) {
                    buttons += `
                        <button type="button" class="btn btn-outline-danger btn-hapus" data-nama="${data.login_name}" data-id="${data.id}">
                            <i class="bi bi-trash-fill"></i>
                            Hapus
                        </button>
                    `;
                }

                return buttons;
            }
        }
    ],
    drawCallback: function () {
        initEventHandler();
        hideLoading();
    },
});

function initEventHandler() {
    // Proses Menghapus data
    $(".btn-hapus").click(function (e) {
        e.preventDefault();

        // Inisiasi variabel yang dibutuhkan
        let id = $(this).attr("data-id");
        let nama = $(this).attr("data-nama");

        // Pop up confirmation
        Swal.fire({
            title: `Apa kamu yakin ingin menghapus data User ${nama}?`,
            text: "Anda tidak akan dapat mengembalikan ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
            if (result.isConfirmed) {

                // Memanggil API Hapus Data
                $.ajax({
                    type: "DELETE",
                    url: api(`/user/${id}`),
                    headers: {
                        'Authorization': getBearerToken()
                    },
                    success: function () {
                        Swal.fire(
                            'Dihapus!',
                            `User ${nama} telah dihapus.`,
                            'success'
                        ).then(() => {
                            datatables.ajax.reload();
                        })
                    }
                });

            }
        })
    });

    // Proses Download export data user dalam bentuk excel
    $("#export-button").click(function (e) {
        e.preventDefault();
        window.open(api('/user/export/excel'), '_blank');
    });

}