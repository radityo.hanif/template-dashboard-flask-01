$("#submit-button").click(function (e) { 
    e.preventDefault();
    
    let url = api("/register");
    let formData = getFormData();

    $.ajax({
        type: "POST",
        url: url,
        timeout: 0,
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        dataType: "json",
        headers: {
            'Authorization': getBearerToken()
        },
        data: formData,
        beforeSend: function() {
            showLoading();
        },
        success: function () {
            localStorage.setItem("success_message", "Berhasil menambahkan data");
            window.location.replace(base_url("/dashboard/user"));
        },
        error: function(response) {
            let res = response.responseJSON;
            showErrorMessage(res.message);
        },
        complete: function() {
            hideLoading();
        }
    });
});