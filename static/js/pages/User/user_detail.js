$(document).ready(function () {
    let id = getParam("id");
    $.ajax({
        type: "GET",
        url: api(`/user/${id}`),
        headers: {
            'Authorization': getBearerToken()
        },
        success: function (response) {
            setDataDetail(response.data);
        }
    });
});