$("#table1").DataTable({
    searching: true,
    dom: 'Bfrtip',
    responsive: true,
    buttons: [
        'excelHtml5',
        'pdfHtml5'
    ],
    initComplete: function() {
        $(".buttons-pdf").addClass("btn btn-outline-danger").html(`<i class="fs-5 bi bi-filetype-pdf"></i> Export to PDF`);
        $(".buttons-excel").addClass("btn btn-outline-success").html(`<i class="fs-5 bi bi-filetype-xlsx"></i> Export to Excel`);
    }
});