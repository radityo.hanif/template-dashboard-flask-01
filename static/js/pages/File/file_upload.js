$("#submit-button").click(function (e) { 
    e.preventDefault();
    
    $.ajax({
        type: "POST",
        url: api("/file-upload"),
        timeout: 0,
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        dataType: "json",
        headers: {
            'Authorization': getBearerToken()
        },
        data: getFormData(),
        beforeSend: function() {
            showLoading();
        },
        success: function () {
            showSuccessMessage("Berhasil mengupload file");
            // localStorage.setItem("success_message", "Berhasil mengupload file");
            // window.location.replace(base_url("/dashboard/file/upload"));
        },
        error: function(response) {
            let res = response.responseJSON;
            showErrorMessage(res.message);
        },
        complete: function() {
            hideLoading();
        }
    });

});