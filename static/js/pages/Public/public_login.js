$("#submit-button").click(function (e) { 
    e.preventDefault();
    
    let url = api("/login");
    let formData = getFormData();

    $.ajax({
        type: "POST",
        url: url,
        timeout: 0,
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        dataType: "json",
        data: formData,
        beforeSend: function() {
            showLoading();
        },
        success: function (response) {
            showSuccessMessage("Berhasil login, mohon tunggu sebentar");
            setCookie("X-JWT-TOKEN", response.jwt);
            localStorage.setItem("user", JSON.stringify(response.payload));
            window.location.replace(base_url("/login"));
        },
        error: function(response) {
            let res = response.responseJSON;
            showErrorMessage(res.message);
        },
        complete: function() {
            hideLoading();
        }
    });
});