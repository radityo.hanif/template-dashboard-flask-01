$("#submit-button").click(function (e) { 
    e.preventDefault();
    
    let url = api("/register");
    let formData = getFormData();

    $.ajax({
        type: "POST",
        url: url,
        timeout: 0,
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        dataType: "json",
        data: formData,
        beforeSend: function() {
            showLoading();
        },
        success: function (response) {
            // cek apakah ada action dari backend
            if(response.action) { 
                localStorage.setItem(response.action, response.message);
            } 
            else {
                localStorage.setItem("success_message", "Berhasil melakukan registrasi, silahkan lakukan proses login pada halaman ini");
            }
            window.location.replace(base_url("/login"));
        },
        error: function(response) {
            let res = response.responseJSON;
            showErrorMessage(res.message);
        },
        complete: function() {
            hideLoading();
        }
    });
});