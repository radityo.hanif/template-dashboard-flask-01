/**
 * File javascript yang akan dijalankan saat halaman pertama kali di load
 * pada file ini kita dapat mendefinisikan proses-proses yang harus dikerjakan terlebih dahulu sebelum mengerjakan proses lainnya
 */

/**
 * Menampilkan notifikasi error atau sukses
 * ketika halaman pertama kali di load
 */
function initFlashMessage() {
    let key;
    
    key = "success_message";
    if(localStorage.getItem(key) != 0) {
        showSuccessMessage(localStorage.getItem(key));
        localStorage.setItem(key, 0);
    }
    
    key = "error_message";
    if(localStorage.getItem(key) != 0) {
        showErrorMessage(localStorage.getItem(key));
        localStorage.setItem(key, 0);
    }
}
initFlashMessage();