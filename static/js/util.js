/*
    util.js
    ini adalah file javascript yang dipanggil di setiap halaman pada aplikasi
    kamu bisa menggunakan file ini untuk mendefinisikan fungsi-fungsi umum
 */

function getMessage(key) {
    let messageList = {
        "expired_session" : "Sesi kamu telah habis silahkan login kembali",
        // silahkan tulis jenis pesan lainnya disini
    };
    return messageList[key];
}

function base_url(endpoint = "/") {
    /**
     * How to use :
     * api("/register")
     * api("/login")
     * 
     * Description :
     * return formatted url
     */
    return BASE_URL + endpoint;
}

function api(endpoint) {
    /**
     * How to use :
     * api("/register")
     * api("/login")
     * 
     * Description :
     * return formatted url
     */
    return BACKEND_URL + endpoint;
}

function getFormData(selector = "#form") {
    /**
     * How to use :
     * let formData = getFormData();
     * let formData = getFormData("#custom-selector");
     * 
     * Description :
     * convert object to formData
     */
    let formData = new FormData();
    let data = $(selector).serializeObject();
    Object.keys(data).forEach(key => {
        formData.append(key, data[key])
    });

    // Check if there any file uploads
    let fileUpload = $("input[type=file]")
    if(fileUpload.length > 0) {
        for(let i=0; i<fileUpload.length; i++) {
            let uFile = fileUpload[i].files[0];
            let kFile = $(fileUpload[i]).attr("id");
            formData.append(kFile, uFile); 
        }
    }
    
    return formData;
}

function createToast(message, color, position="center") {
    /**
     * Membutuhkan Library toastify
     * untuk menjalankan fungsi ini
     * cara mengaktifkan :
        * tambahkan konfigurasi ini pada routing flask 
            data["enable_extensions"] = ["sweetalert", "toastify"]
     */
    Toastify({
        text: message,
        // duration: 3000,
        duration: -1,
        close: true,
        position: position,
        backgroundColor: color,
    }).showToast();
}

function showErrorMessage(message) {
    let color = "linear-gradient(to right, #FF4B2B, #FF416C)";
    createToast(message, color);
}

function showSuccessMessage(message) {
    let color = "linear-gradient(to right, #00b09b, #96c93d)";
    createToast(message, color);
}

function setCookie(name,value,days=1) {
    // reference : https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    // reference : https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name="X-JWT-TOKEN") {   
    // reference : https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function getBearerToken() {
    let token = getCookie("X-JWT-TOKEN");
    let error_message = getMessage("expired_session");
    if(token == null) {
        localStorage.setItem("error_message", error_message);
        window.location.replace(base_url('/login'));
    } else {
        return `Bearer ${token}`;
    }
}

function loginRequired(value) {
    /**
     * Fungsi middleware autentikasi
     */
    let token = getCookie("X-JWT-TOKEN");
    let error_message = getMessage("expired_session");
    if(value == true) {
        // cek apakah client sudah memiliki token
        if(token == null) {
            localStorage.setItem("error_message", error_message);
            window.location.replace(base_url('/login'));
        }
        // cek apakah token client valid
    } else if(value == false) {
        // cek apakah client memiliki token
        if(token != null) {
            window.location.replace(base_url('/dashboard'));
        }
    }
}

function getDataUser(key) {
    let dataUser = JSON.parse(localStorage.getItem("user"));
    return dataUser[key];
}

function getParam(key) {
    let url_string = window.location.href; 
    let url = new URL(url_string);
    let param = url.searchParams.get(key);
    return param;
}

function setDataDetail(data) {
    /**
     * Fungsi ini digunakan untuk mapping data dari api ke value dari element yang memiliki id yang sama dengan property pada response api
     * Fungsi ini bisa digunakan untuk mempercepat proses pembuatan halaman detail standar
     * contoh penggunaan :
        * setDataDetail(response.data)
     */
    let values = data;
    let keys = Object.keys(data);
    let length = keys.length;

    for(let i=0; i<length; i++) {
        $(`#${keys[i]}`).val(values[keys[i]]);
    }
}