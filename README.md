## Description
Web Dashboard Flask dibuat menggunakan template https://zuramai.github.io/mazer/demo/index.html

## License
- gratis untuk komersil
- siapapun bisa menggunakan source code ini 😁

## Author
radityo.hanif follow me in 
- instagram 👉 https://instagram.com/radityo.hanif
- gitlab    👉 https://gitlab.com/radityo.hanif

## Feature
- sistem login menggunakan document.cookie melainkan frontend akan menyimpan jwt token pada web cookies di sisi javascript
- penamaan template html secara default akan sesuai dengan nama fungsi dan prefix yang didefinisikan misalnya, pada routes dashboard memiliki prefix "db_", terdapat fungsi bernama index maka nama templatenya adalah db_index.html
- sudah terintegrasi dengan library css bootstrap 5
- sudah terintegrasi dengan library javascript yang sering digunakan seperti jquery, datatables, apache echart dan lainnya

## Working Folder
- **static/css**            👉 tempat kita menyimpan seluruh stylesheet
- **static/extensions**     👉 tempat kita menyimpan library yang ingin digunakan pada aplikasi
- **static/images**         👉 tempat kita menyimpan gambar statis atau asset yang ingin digunakan pada - aplikasi (jpg, png, svg, dll)
- **static/js**             👉 tempat kita menyimpan koding javascript
- **routes/Public**         👉 seluruh halaman root yang diawali dengan /
- **routes/Dashboard**      👉 seluruh halaman yang terisolasi diakses setelah login, atau client yang memiliki jwt token

## Installation
setup file .env kamu
```bash
    pip install -r requirements.txt
```
```bash
    python run.py
```